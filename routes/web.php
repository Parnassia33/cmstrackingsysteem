<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TrackingNumberController;
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', function () {
    $order_number = 'ordernumber';
    $email = 'email';
    $zipcode = 'zipcode';
    return view('orderWelcome', compact('order_number', 'email', 'zipcode'));
});
Route::match(['get','post'],"/trackinginformation/{order_number}/{email}/",'TrackingNumberController@saveApiData')->name('order.number');

// vanaf hier komt het admin gedeelte
Route::resource('admin' ,'carriersController')->middleware('auth');

Route::resource('blog' ,'BlogController');

//dit is de route voor de statussen
Route::resource('status' ,'statusController')->middleware('auth');
//dit is de route voor de substatussen
Route::resource('substatus', 'subStatusController')->middleware('auth');
//dit is de route voor de substatusdescriptions
Route::resource('substatusdescription', 'SubStatusDescriptionController')->middleware('auth');

//De api route
Route::get('/apicarriers', 'carriersController@getApi')->name('api.carrier.list');

Route::get('/time', 'TrackingNumberController@time')->name('time');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
