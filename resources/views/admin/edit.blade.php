@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit current carrier</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('admin.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="card">
    {!! Form::model($carrier, ['method' => 'PATCH','route' => ['admin.update', $carrier->id]]) !!}
        @csrf
        <label>Name:</label>
        <input type="text" class="form-control" name="name" id="name" value="{{$carrier->name}}">
        <label>Code:</label>
        <input type="text" class="form-control" name="code" id="code" value="{{$carrier->code}}">
        <label>Country</label>
        <select value="{{old('country')}}" name="country" class="form-control">
            <option value="">{{$carrier->country}}</option>
            @foreach ($countries as $countries)
                <option value="{{$countries->name}}">{{$countries->name}}</option>
            @endforeach
        </select><span class="pmd-textfield-focused"></span>

        <label>Type:</label>
        <input type="text" class="form-control" name="type" id="type" value="{{$carrier->type}}">

        <label>Type:</label>
        <input type="text" class="form-control" name="type" id="type" value="{{$carrier->homepage}}">

        <label>Type:</label>
        <input type="text" class="form-control" name="type" id="type" value="{{$carrier->phone}}">

        <label>Type:</label>
        <input type="text" class="form-control" name="type" id="type" value="{{$carrier->picture}}">

        <button type="submit" class="btn btn-primary">Submit
        </button>
    </form>
    {!! Form::close() !!}
    </div>
@endsection
