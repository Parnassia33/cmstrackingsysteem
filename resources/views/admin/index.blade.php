@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Carriers</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{route('api.carrier.list')}}" onclick="loader()">Get a full updated list from carriers</a>
            </div>
        </div>
    </div>
    <div class="spinner spinner-border text-primary" id="loader" hidden></div>
    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>Name</th>
            <th>Code</th>
            <th>Country</th>
            <th>Type</th>
            <th>Homepage</th>
            <th>Phone</th>
            <th>Picture</th>
            <th>Action</th>
        </tr>
        @foreach ($carriers as $carrier)
            <tr>
                <td>{{ $carrier->name }}</td>
                <td>{{ $carrier->code }}</td>
                <td>{{ $carrier->country }}</td>
                <td>{{ $carrier->type }}</td>
                <td><a href="{{ $carrier->homepage }}">{{ $carrier->name }}</a> </td>
                <td>{{ $carrier->phone }}</td>
                <td><img src="{{ $carrier->picture }}" height="100px" width="100px"> </td>
                <td>
                    {!! Form::open(['method' => 'DELETE','route' => ['admin.destroy', $carrier->id],'style'=>'display:inline']) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}

                        <a class="btn btn-primary" href="{{ route('admin.edit',$carrier->id) }}">Edit</a>
                </td>
            </tr>
        @endforeach
    </table>
    {{ $carriers->links('pagination::bootstrap-4') }}
@endsection
