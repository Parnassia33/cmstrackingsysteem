
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('admin.index') }}"> Back</a>
                </div>
                <div class="card">
                    <div class="card-body">
                        <p class="font-weight-bold">
                            Name: {{ $carrier->name }}
                            <br>
                            code : {{$carrier->code}}
                            <br>
                            country : {{ $carrier->country}}
                            <br>
                            Type : {{$carrier->type}}
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
