@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add New Carrier</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('admin.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('admin.store') }}" method="POST">
        @csrf
        <label>Name:</label>
        <input type="text" class="form-control" name="name" id="name" value="{{old('name')}}">

        <label>Code:</label>
        <input type="text" class="form-control" name="code" id="code" value="{{old('code')}}">

        <label>Country</label>
        <select value="{{old('country')}}" name="country" class="form-control">
            <option value="">Select Country</option>
            @foreach ($countries as $countries)
                <option value="{{$countries->name}}">{{$countries->name}}</option>
            @endforeach
        </select><span class="pmd-textfield-focused"></span>

        <label>Homepage:</label>
        <input type="text" class="form-control" name="homepage" id="homepage" value="{{old('homepage')}}">

        <label>Type:</label>
        <input type="text" class="form-control" name="type" id="type" value="{{old('type')}}">

        <label>Phone:</label>
        <input type="text" class="form-control" name="phone" id="phone" value="{{old('phone')}}">

        <label>Picture:</label>
        <input type="text" class="form-control" name="picture" id="picture" value="{{old('picture')}}">

        <button type="submit" class="btn btn-primary">Submit
        </button>
    </form>
@endsection
