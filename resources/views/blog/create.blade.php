@extends('layouts.create')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add New Question</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('blog.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('blog.store') }}" method="POST">
        @csrf
        <label>Title:</label>
        <input type="text" class="form-control" name="title" id="title" value="{{old('title')}}">

        <label>Question:</label>
        <input type="text" class="form-control" name="question" id="question" value="{{old('question')}}">

        <label>Answer:</label>
        <input type="text" class="form-control" name="answer" id="answer" value="{{old('answer')}}">

        <label>Urgency:</label>
        <input type="text" class="form-control" name="urgency" id="urgency" value="{{old('urgency')}}" placeholder="1 is the highest urgency">

        <button type="submit" class="btn btn-primary">Submit
        </button>
    </form>
@endsection
