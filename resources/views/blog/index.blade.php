@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>FAQ</h2>
            </div>
        </div>
    </div>
    @if (Auth::check())
    <div class="pull-right">
        <a class="btn btn-success" href="{{ route('blog.create') }}"> Create blog</a>
    </div>
    @endif

    {!! Form::open(['method'=>'GET','url'=>'blog','class'=>'navbar-form navbar-left','role'=>'search'])  !!}
    <div class="input-group custom-search-form">
        <input type="text" class="form-control" name="search" placeholder="Search...">
        <span class="input-group-btn">
        <a class="btn btn-success"> Search</a>
</span>
        {!! Form::close() !!}
    @foreach ($blog as $blogs)
        <br>
        <br>
    <div class="card" style="position: relative;
    display: inline-table;
    flex-wrap: wrap;
    align-items: stretch;
    width: 100%;}">
        <div class="card">
        <div class="card-header">
            <a href="{{route('blog.show', $blogs->id)}}"><h5 style="color: #1b1e21" class="card-title"><b>{{$blogs->title}}</b></h5></a>
        </div>
        <div class="card-body" style="color: black">{{$blogs->question}}</div>
        @if (Auth::check())
        <div class="card-footer">
            <a class="btn btn-primary" href="{{ route('blog.edit',$blogs->id) }}">Edit</a>
            {!! Form::open(['method' => 'DELETE','route' => ['blog.destroy', $blogs->id],'style'=>'display:inline']) !!}
            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
        </div>
            @endif
    </div>

     @endforeach
@endsection
