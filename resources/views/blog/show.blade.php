@extends('layouts.create')

@section('content')

    <div class="pull-right">
        <a class="btn btn-primary" href="{{ route('blog.index') }}"> Back</a>
    </div>

    <div class="card" style="width: 80rem;
                             text-align: center;
                             position:absolute;
                             right:250px;
                             top:250px;">
        <div class="card-header">
        <h5 class="card-title" style="color: black;">{{$blog->question}}</h5>
        </div>
        <div class="card-body">
            <p class="card-text" style="color: black">{{$blog->answer}}</div>
    </div>
@endsection
