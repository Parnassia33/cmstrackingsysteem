@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit {{$blog->title}}</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('blog.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="card">
        {!! Form::model($blog, ['method' => 'PATCH','route' => ['blog.update', $blog->id]]) !!}
        @csrf
        <label>Title:</label>
        <input type="text" class="form-control" name="title" id="title" value="{{$blog->title}}">
        <label>Question:</label>
        <input type="text" class="form-control" name="question" id="question" value="{{$blog->question}}">
        <label>Answer::</label>
        <input type="text" class="form-control" name="answer" id="answer" value="{{$blog->answer}}">
        <button type="submit" class="btn btn-primary">Submit
        </button>
        </form>
        {!! Form::close() !!}
    </div>
@endsection
