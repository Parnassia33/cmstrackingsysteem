@extends('layouts.create')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add New substatus</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('substatus.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('substatus.store') }}" method="POST">
        @csrf
        <label>Code:</label>
        <input type="text" class="form-control" name="substatuscode" id="substatuscode" value="{{old('substatuscode')}}">
        <label>status code:</label>
        <select value="{{old('status_code')}}" name="status_code" id="status_code" class="form-control">
            @foreach ($status as $statuses)
                <option value="{{$statuses->code}}">{{$statuses->code}}</option>
            @endforeach
        </select><span class="pmd-textfield-focused"></span>
                <label>Description:</label>
                <input type="text" class="form-control w-50" name="description" id="description" value="{{old('description')}}" size="50">
        <br>
        <button type="submit" class="btn btn-primary">Submit
        </button>
    </form>
@endsection
