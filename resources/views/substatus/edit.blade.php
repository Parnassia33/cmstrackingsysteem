@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit current substatus</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('substatus.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="card">
    {!! Form::model($substatus, ['method' => 'PATCH','route' => ['substatus.update', $substatus->id]]) !!}
        @csrf
        <label>Code:</label>
        <input type="text" class="form-control" name="code" id="code" value="{{$substatus->code}}">
        <label>Description</label>
        <input type="text" class="form-control" name="description" id="description" value="{{$substatus->description}}">
        <button type="submit" class="btn btn-primary">Submit
        </button>
    </form>
    {!! Form::close() !!}
    </div>
@endsection
