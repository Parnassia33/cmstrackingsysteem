@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Substatus</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('substatus.create') }}"> Create substatus</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>Code</th>
            <th>Description</th>
            <th>Hours</th>
            <th>Action</th>
        </tr>
        @foreach ($substatuses as $substatus)
            <tr>
                <td>{{ $substatus->status_code }}</td>
                <td>{{ $substatus->description }}</td>
                <td>{{ $substatus->hours ?? 'Instantly' }}</td>
                <td>
                    {!! Form::open(['method' => 'DELETE','route' => ['substatus.destroy', $substatus->id],'style'=>'display:inline']) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}

                    <a class="btn btn-primary" href="{{ route('substatus.edit',$substatus->id) }}">Edit</a>
                </td>
            </tr>
    @endforeach
    </table>
    {{ $substatuses->links('pagination::bootstrap-4') }}
@endsection
