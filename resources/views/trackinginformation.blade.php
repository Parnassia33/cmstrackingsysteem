@extends('layouts.app')

@section('content')
    <body>
    <style>
        .loader {
            position: fixed;
            z-index: 99;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background: #87cefc;
            display: flex;
            justify-content: center;
            align-items: center;
        }

        .loader > img {
            width: 100px;
        }

        .loader.hidden {
            animation: fadeOut 1s;
            animation-fill-mode: forwards;
        }

        @keyframes fadeOut {
            100% {
                opacity: 0;
                visibility: hidden;
            }
        }
    </style><div class="loader">
        <img src="/images/PreLoaders.gif" class="spinner" class="loader">
    </div>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />

    <div class="flex-center position-ref full-height">
             <div class="content">
              <div class="card">
                 <table class="table">
                     <thead>
                     <tr>
                         <th scope="col"><b>Tracking number:</b></th>
                         <th scope="col"><b>Status:</b></th>
                         <th scope="col"><b>Product:</b></th>
                         <th scope="col"><b>Variation:</b></th>
                         <th scope="col"><b>Image:</b></th>
                     </tr>
                     </thead>
                     <tbody>
                     @foreach($returnArray as $k => $trackingArray)
                         <tr>
                             <td>{{$trackingArray['tracking_nr'] ?? ''}}</td>
                             <td>{{$trackingArray['status'] ?? ''}}</td>
                             <td>{{$trackingArray['productTitle'] ?? ''}}</td>
                             <td>{{$trackingArray['productVariation'] ?? ''}}</td>
                             <td><img src="{{$trackingArray['productImage'] ?? ''}}" height="100" width="100"></td>
                         </tr>
                         <tr>
                             <td colspan="5">
                                 <div class="progress">
                                     <div class="progress-bar progress-bar-striped" role="progressbar" style="width: {{$trackingArray["progress"]}}%" aria-valuenow="{{$trackingArray["progress"]}}" aria-valuemin="0" aria-valuemax="100"></div>
                                 </div>
                                 <b>{{$trackingArray["progress"]}}%</b>
                             </td>
                         </tr>
                         <tr>
                             <td colspan="5">
                                 <div class="status" style="text-align: center">
                                     <b>{{$trackingArray["normalStatus"]}}</b>
                                 </div>
                             </td>
                         </tr>
                     @endforeach
                     </tbody>
                 </table>
             </div>
            </div>
           </div>
    <style>
        .card {
            background-color: #87cefc;
        }
    </style>
    <script>
        window.addEventListener("load", function () {
            const loader = document.querySelector(".loader");
            loader.className += " hidden"; // class "loader hidden"
        });
    </script>
    </body>
    </html>
@endsection
