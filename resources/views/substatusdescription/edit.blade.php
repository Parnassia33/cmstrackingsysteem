@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit {{$substatusdescription->substatuscode}} Description</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('substatusdescription.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="card">
    {!! Form::model($substatusdescription, ['method' => 'PATCH','route' => ['substatusdescription.update', $substatusdescription->id]]) !!}
        @csrf
        <label>status code:</label>
        <select value="{{old('substatuscode')}}" name="substatuscode" id="substatuscode" class="form-control">
            <option value="{{$substatusdescription->substatuscode}}">{{$substatusdescription->substatuscode}}</option>
            @foreach ($substatus as $substatuses)
                <option value="{{$substatuses->code}}">{{$substatuses->code}}</option>
            @endforeach
        </select><span class="pmd-textfield-focused"></span>
        <br>


        <label>Description</label>
        <div class="row">
            <input type="text" class="form-control w-50 r-0" name="description" id="description" value="{{$substatusdescription->description}}">
            <input type="number" class="form-control w-25 r-0" name="hours" id="hours" value="{{$substatusdescription->hours}}">
        </div>
        <br>
        <label>Percentage %</label>
        <input type="number" class="form-control w-25 r-0" name="hours" id="hours" value="{{$substatusdescription->percentage}}">
        <button type="submit" class="btn btn-primary">Submit
        </button>
    </form>
    {!! Form::close() !!}
    </div>
@endsection
