@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Substatus Description</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{route('substatusdescription.create')}}"> Create a new Substatus Description!</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>Code</th>
            <th>Description</th>
            <th>Hours</th>
            <th>Percentage</th>
            <th>Action</th>
        </tr>
        @foreach ($subStatusDescriptions as $subStatusDescription)
            <tr>
                <td>{{ $subStatusDescription->substatuscode }}</td>
                <td>{{ $subStatusDescription->description }}</td>
                <td>{{ $subStatusDescription->hours}}</td>
                <td>{{ $subStatusDescription->percentage}}%</td>
                <td>
                    {!! Form::open(['method' => 'DELETE','route' => ['substatusdescription.destroy', $subStatusDescription->id],'style'=>'display:inline']) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}
                    <a class="btn btn-primary" href="{{ route('substatusdescription.edit',$subStatusDescription->id) }}">Edit</a>
                </td>
            </tr>
    @endforeach
    </table>
    {{ $subStatusDescriptions->links('pagination::bootstrap-4') }}
@endsection
