@extends('layouts.create')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add New Substatus Description!</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('substatusdescription.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('substatusdescription.store') }}" method="POST">
        @csrf
        <label>SubStatus code:</label>
        <select value="{{old('substatuscode')}}" name="substatuscode" id="substatuscode" class="form-control" >
            <option value="">Select Substatus code</option>
            @foreach ($substatus as $substatuses)
                <option value="{{$substatuses->substatuscode}}">{{$substatuses->substatuscode}}</option>
            @endforeach
        </select><span class="pmd-textfield-focused"></span>
        <br>
        <label>Description:</label>
        <br>
        <div class="row">
                <input type="text" class="form-control w-75 r-0" name="description" id="description"  style="margin: 0px; padding: 0px;" value="{{old('Description')}}">
                <input type="number" class="form-control w-25 r-0" name="hours" value="{{old('hours')}}" placeholder="Hours">
        </div>
        <br>
        <label>Percentage %</label>
        <input type="number" class="form-control w-25 r-0" name="percentage" value="{{old('percentage')}}" placeholder="percentage">

        <button type="submit" class="btn btn-primary">Submit
        </button>
    </form>
@endsection
