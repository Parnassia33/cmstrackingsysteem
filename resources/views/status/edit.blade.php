@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit current Status</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('status.index') }}"> Back</a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="card">
    {!! Form::model($status, ['method' => 'PATCH','route' => ['status.update', $status->id]]) !!}
        @csrf
        <label>Code:</label>
        <input type="text" class="form-control" name="code" id="code" value="{{$status->code}}">
        <label>Description</label>
        <input type="text" class="form-control" name="description" id="description" value="{{$status->description}}">
        <button type="submit" class="btn btn-primary">Submit
        </button>
    </form>
    {!! Form::close() !!}
    </div>
@endsection
