@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Status</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('status.create') }}"> Create status</a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered">
        <tr>
            <th>Code</th>
            <th>Description</th>
            <th>Action</th>
        </tr>
        @foreach ($statuses as $status)
            <tr>
                <td>{{ $status->code }}</td>
                <td>{{ $status->description }}</td>
                <td>
                    {!! Form::open(['method' => 'DELETE','route' => ['status.destroy', $status->id],'style'=>'display:inline']) !!}
                    {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                    {!! Form::close() !!}

                    <a class="btn btn-primary" href="{{ route('status.edit',$status->id) }}">Edit</a>
                </td>
            </tr>
    @endforeach
    </table>
    {!! $statuses->links('pagination::bootstrap-4') !!}
@endsection
