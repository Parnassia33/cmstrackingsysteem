@extends('layouts.app')

@section('content')
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title> Tracking System</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>

            .full-height {
                height: 75vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .news-scroll a {
                text-decoration: none;
                bottom: 0;
            }
        </style>
        <div class="flex-center position-ref full-height">
            <div class="content">
                <div class="title m-b-md">
                   Order number:
                </div>
                    <form action="{{route('order.number',['order_number' => $order_number, 'email' => $email])}}" method="POST">
                        @csrf
                        <input type="text" class="form-control" name="order_number" id="order_number" value="{{old('order_number')}}" placeholder="Order number here">
                        <br>
                            <h1 id="emailOrZipcode">E-mail</h1>
                            <input type="text" class="form-control" name="email" id="email" value="{{old('email')}}" placeholder="E-mail here">
                            <button type="submit" class="btn btn-primary">Submit
                            </button>
                        <br>
                        <br>
                    </form>
            </div>
        </div>
        <footer>
            <div class="row">
                <div class="col-md-12">
                        <marquee class="news-scroll" behavior="scroll" onmouseover="this.stop();" onmouseout="this.start();">
                            <img height="70px" src="//s.trackingmore.com/images/icons/express/dhl.png">
                            <img height="70px" src="//s.trackingmore.com/images/icons/express/ups.png">
                            <img height="70px" src="//s.trackingmore.com/images/icons/express/fedex.png">
                            <img height="70px" src="//s.trackingmore.com/images/icons/express/tnt.png">
                            <img height="70px" src="//s.trackingmore.com/images/icons/express/companylogo/21050.jpg">
                            <img height="70px" src="//s.trackingmore.com/images/icons/express/companylogo/14040.jpg">
                            <img height="70px" src="//s.trackingmore.com/images/icons/express/companylogo/3040.jpg">
                            <img height="70px" src="//s.trackingmore.com/images/icons/express/companylogo/7040.jpg">
                            <img height="70px" src="//s.trackingmore.com/images/icons/express/dpd-be.png">
                            <img height="70px" src="//s.trackingmore.com/images/icons/express/hound.png">
                            <img height="70px" src="//s.trackingmore.com/images/icons/express/easy-mail.png">
                            <img height="70px" src="//s.trackingmore.com/images/icons/express/sgt-it.png">
                            <img height="70px" src="//s.trackingmore.com/images/icons/express/lasership.png">
                            <img height="70px" src="//s.trackingmore.com/images/icons/express/arrowxl.png">
                            <img height="70px" src="//s.trackingmore.com/images/icons/express/i-parcel.png">
                            <img height="70px" src="//s.trackingmore.com/images/icons/express/asendia-de.png">
                            <img height="70px" src="//s.trackingmore.com/images/icons/express/bondscouriers.png">
                            <img height="70px" src="//s.trackingmore.com/images/icons/express/toll-ipec.png">
                            <img height="70px" src="//s.trackingmore.com/images/icons/express/companylogo/10010.jpg">
                            <img height="70px" src="//s.trackingmore.com/images/icons/express/dayross.png">
                            <img height="70px" src="//s.trackingmore.com/images/icons/express/companylogo/90071.jpg">
                            <img height="70px" src="//s.trackingmore.com/images/icons/express/delhivery.png">
                      </marquee>
                </div>
            </div>
        </footer>
@endsection
