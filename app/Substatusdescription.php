<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Substatusdescription extends Model
{
    use HasFactory;

    protected $fillable = [
        'substatuscode','hours','description','percentage'
    ];
}
