<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tracking extends Model
{
    use HasFactory;
    public $timestamps = false;
    protected $fillable = [
        'email','orderNumber','trackingNumber','createdAt','lastEvent', 'productTitle' , 'productImage'
        , 'productVariation' , 'carrier'
    ];
}
