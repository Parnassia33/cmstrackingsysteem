<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:256',
            'question' => 'required|min:10|',
            'answer' => 'required|min:5',
        ];
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'The title is required',
            'question.required' => 'A question is required',
            'answer.required' => 'A answer is required',
            'title.max' => 'the max count of letters for a title is 256',
            'question.min' => 'the minimum count of letters for a question is 10',
            'answer.min' => 'the minimum count of letters for a answer is 5'
        ];
    }
}
