<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'substatuscode' => 'required',
            'status_code' => 'required',
            'description' => 'required',
        ];
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'substatuscode.required' => 'The substatuscode is required',
            'status_code.required' => 'Pick a status code',
            'status_code.required' => 'Make a description',
        ];
    }
}
