<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SubstatusDescriptionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules()
    {
        return [
            'substatuscode' => 'required',
            'hours' => 'required|integer',
            'description' => 'required|string|min:10',
            'percentage' => 'required|integer|max:100',
        ];
    }
    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'substatuscode.required' => 'The substatuscode is required',
            'hours.required' => 'hours must be numeric ',
            'description.required' => 'A description is required',
            'percentage.required' => 'Percentage must be numeric ',
            'percentage.max' => 'Percentage must be a max of 100% ',
        ];
    }
}
