<?php

namespace App\Http\Controllers;

use App\Http\Requests\StatusRequest;
use App\Status;
use Illuminate\Http\Request;

class statusController extends Controller
{
    public function index()
    {
        $statuses = Status::paginate(5);
//            ->with('tags')
//            ->orderBy('id', 'ASC')
//            ->paginate(3);
        return view('status.index', compact('statuses'));
    }

    public function destroy($id)
    {
        Status::find($id)->delete();

        return redirect()->route('status.index')
            ->with('success','Product deleted successfully');
    }

    public function create()
    {
//        $this->authorize('create');
        return view('status.create');
    }

    /**
     * Store with regex
     *
     * @param  \App\Http\Requests\StatusRequest  $request
     * @return Illuminate\Http\Response
     */
    public function store(StatusRequest $request)
    {
        Status::create($request->all());

        return redirect()->route('status.index')
            ->with('success','Product created successfully.');
    }

    public function edit($id)
    {
        $status = Status::find($id);
        return view('status.edit',compact('status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(Request $request,$id)
    {
        Status::find($id)->update($request->all());

        return redirect()->route('status.index')
            ->with('success','Item updated successfully');
    }

    public function substatus($id)
    {
        $status = Status::find($id);
        return view('status.substatus');
    }

}
