<?php

namespace App\Http\Controllers;

use App\Tracking_number;
use App\Tracking;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

class TrackingNumberController extends Controller
{
//    public function Token()
//    {
//        $token = '8e37f159-27ca-4f99-9ecb-1b81e718a78e';
//
//        $hashToken = hash('sha256', $token);
//
//        return $hashToken;
//    }

    public function saveApiData(Request $request)
    {
        $order_number = $request->input('order_number');
        $email = $request->input('email');
        $token = '8e37f159-27ca-4f99-9ecb-1b81e718a78e';
//        $token = $this->Token();
//        dd($token);
        $response = Http::withHeaders([
                'Accept' => 'application/json',
                'Api-Key' => $token,
                'Content-Type' => 'application/json',
            ])->post('https://search.trizty.com/tracking/v1/trizty/gettracking', ["ordernr" => $order_number,
                "email" => $email]);
        $codeResponse = $response->status();
            if ($codeResponse == 404) {
                return Redirect::back()->withErrors(['Failed to find that resource']);
            }
            if ($codeResponse == 400)
            {
                return Redirect::back()->withErrors(['Check your email and order number please!']);
            }
        $trackingResponse = $response['tracking'];
        $orderDate = $trackingResponse['order_date'];
        $orderStatus = $trackingResponse['order_status'];
        $orderFulfillment = $trackingResponse['order_fulfillment'];
        $trackingInsideTracking = $trackingResponse['tracking'];
        $token = '3023e896-2e50-4ebb-80a9-8923e1fb47b8';
        $returnArray = array();

        foreach ($trackingInsideTracking as $k => $tracking_numbers) {
            $returnTracking = array();
            $productTitle = $tracking_numbers['product_title'];
            $productImage = $tracking_numbers['product_image'];
            $productVariation = $tracking_numbers['product_variation'];
            $trackingNr = $tracking_numbers['tracking_nr'];
            if (empty($trackingNr))
            {
                $returnTracking["normalStatus"] = "Pending";
                $returnTracking["progress"] = 10 ;
                $returnTracking["productImage"] = $productImage;
                $returnTracking["productTitle"] = $productTitle;
                $returnTracking["productVariation"] = $productVariation;
                $returnTracking["tracking_nr"] = $trackingNr;
                $returnTracking["status"] = "Package added to track";
                $returnArray[] = $returnTracking;
                return view('trackinginformation', compact('returnArray'));
            }
            $response = Http::withHeaders([
                'Accept' => '*/*',
                'Trackingmore-Api-Key' => $token,
                'Content-Type' => 'application/json',
            ])->post('https://api.trackingmore.com/v2/carriers/detect', ["tracking_number" => $trackingNr]);
            $realResponse = $response['data'];
            $latestResponse = last($realResponse); // Check AW: Multiple cariers
            $carrierCode = $latestResponse['code'];

            Http::withHeaders([
                'Accept' => '*/*',
                'Trackingmore-Api-Key' => $token,
                'Content-Type' => 'application/json',
            ])->put('https://api.trackingmore.com/v2/trackings/post', ["tracking_number" => $trackingNr, "carrier_code" => $carrierCode]
            );
            $fullResponse = Http::withHeaders([
                    'Accept' => '*/*',
                    'Trackingmore-Api-Key' => $token,
                    'Content-Type' => 'application/json',
                ]
            )->get('https://api.trackingmore.com/v2/trackings/get',
                ["tracking_number" => $trackingNr, "carrier_code" => $carrierCode]);
            $dataResponse = $fullResponse->json(['data']);
            $itemsResponse = last($dataResponse['items']);
            $trackingStatus = $itemsResponse['status'];
            $trackingSubStatus = $itemsResponse['substatus'];
            if (empty($trackingSubStatus))
            {
                $trackingSubStatus == $trackingStatus+"001";
                return $trackingSubStatus;
            }
            if ($trackingStatus == "notfound") {
                return Redirect::back()->withErrors(["Your order isn't been found yet, please try again later."]);
            }
            $lastUpdateTime = $itemsResponse['lastUpdateTime'];
            $origin_info = $itemsResponse['origin_info'];
            $track_info = head($origin_info['trackinfo']);
            $status = $track_info['StatusDescription'];
            // dit is voor de trackings tabel
            Tracking::create( // Altijd create
                [   'email' => (strtolower($email)),
                    'orderNumber' => (strtoupper($order_number)),
                    'carrier' => (strtolower($carrierCode)),
                    'trackingNumber' => (strtoupper($trackingNr)),
                    'lastEvent' => $status,
                    'createdAt' => Carbon::now(),
                    'productTitle' => $productTitle,
                    'productImage' => $productImage,
                    'productVariation' => $productVariation,
                ]);


            Tracking_number::updateOrCreate(
                [
                    'tracking_number' => (strtoupper($trackingNr)),
                    'carrier' => (strtolower($carrierCode)),
                    'status' => $track_info['checkpoint_status'],
//                    'substatus' => $fullQueryResultSubStatus,
                    'substatus' => $trackingSubStatus,
                    'statusDescription' => $status,
                    'date' => $track_info['Date'],
                    'details' => $track_info['Details'],
                    'lastUpdateTime' => $lastUpdateTime,
                    'lastEvent' => $status,
                    'orderStatus' => $orderStatus,
                    'orderFulfillment' => $orderFulfillment,
                    'orderDate' => $orderDate,
                ]
            );

            // dit is voor de tracking_numbers tabel
            if ($track_info == null) {
                return Redirect::back()->withErrors(['Failed to find that resource']);
            }
            $querySubstatusDescription = DB::select( DB::raw("SELECT t.createdAt, HOUR(TIMEDIFF(NOW(), t.createdAt)) as hours, n.substatus, IFNULL(s.description, n.lastEvent) as description,
                IFNULL(s.percentage, 0) as percentage
                FROM trackings t
                INNER JOIN tracking_numbers n
                ON t.trackingNumber = n.tracking_number
                LEFT JOIN substatusdescriptions s
                ON s.id = (
                  SELECT s1.id FROM substatusdescriptions s1
                  WHERE n.substatus = s1.substatuscode
                  AND (
                    (
                      (s1.hours <= HOUR(TIMEDIFF(NOW(), t.createdAt))) AND
                      (s1.hours > 0)
                    )
                    OR
                    (
                      (s1.hours = 0) AND
                      (NOT EXISTS (
                        SELECT s2.id FROM substatusdescriptions s2
                        WHERE n.substatus = s2.substatuscode
                        AND s2.hours <= HOUR(TIMEDIFF(NOW(), t.createdAt))
                        AND s2.hours > 0
                      ))
                    )
                  )
                  ORDER BY s1.hours DESC LIMIT 0,1
                )
                WHERE t.email = '$email'
                AND t.orderNumber = '$order_number'
                AND t.trackingNumber = '$trackingNr'
                AND t.carrier = '$carrierCode'
                ORDER BY createdAt LIMIT 0,1") );
            $fullQueryResult = head($querySubstatusDescription);
            $fullQueryResultSubStatus = json_encode($fullQueryResult->substatus, true);
            $fullQueryResultDescription = json_encode($fullQueryResult->description, true);
            $fullQueryResultPercentage = json_encode($fullQueryResult->percentage, true);


            $returnTracking["normalStatus"] = $trackingStatus;
            $returnTracking["progress"] = $fullQueryResultPercentage;
            $returnTracking["productImage"] = $productImage;
            $returnTracking["productTitle"] = $productTitle;
            $returnTracking["productVariation"] = $productVariation;
            $returnTracking["tracking_nr"] = $trackingNr;
//            $returnTracking["status"] = $status;
            $returnTracking["status"] = $fullQueryResultDescription;
            $returnArray[] = $returnTracking;
        }
        return view('trackinginformation', compact('returnArray'));
    }


}
