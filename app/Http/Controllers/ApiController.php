<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Util\Carrier;

class ApiController extends Controller
{
    protected $carrier;

    public function __construct(Carrier $carrier)
    {
        $this->carrier = $carrier;
    }

    public function index()
    {
        // Get all the post
        $posts = $this->carrier->all();
        return view('admin.index', compact('carriers'));
    }

    public function show($id)
    {
        $carrier = $this->carrier->findById($id);

        return view('someview', compact('carrier'));
    }
}
