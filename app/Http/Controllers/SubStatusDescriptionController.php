<?php

namespace App\Http\Controllers;
use App\Http\Requests\SubstatusDescriptionRequest;
use App\Status;
use App\Substatus;
use App\Substatusdescription;
use Illuminate\Http\Request;

class SubStatusDescriptionController extends Controller
{
    public function index()
    {
        $subStatusDescriptions = Substatusdescription::paginate(5);
//            ->with('tags')
//            ->orderBy('id', 'ASC')
//            ->paginate(3);
        return view('substatusdescription.index', compact('subStatusDescriptions'));
    }
//
    public function destroy($id)
    {
        Substatusdescription::find($id)->delete();

        return redirect()->route('substatusdescription.index')
            ->with('success','Substatus Description deleted successfully');
    }
//
    public function create(Substatus $substatus)
    {
//        $this->authorize('create');
        $substatus = Substatus::all();
        $status = Status::all();
        return view('substatusdescription.create', compact('substatus','status'));
    }
//

    /**
     * Store with regex
     *
     * @param SubstatusDescriptionRequest $request
     * @return Illuminate\Http\Response
     */

    public function store(SubstatusDescriptionRequest $request)
    {
        Substatusdescription::create($request->all());

        return redirect()->route('substatusdescription.index')
            ->with('success','Substatus Description created successfully.');
    }

    public function edit($id)
    {
        $substatusdescription = Substatusdescription::find($id);
        $substatus = Substatus::all();
        return view('substatusdescription.edit',compact('substatusdescription', 'substatus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request,$id)
    {
        Substatusdescription::find($id)->update($request->all());

        return redirect()->route('substatusdescription.index')
            ->with('success','Substatus Description updated successfully');
    }

}
