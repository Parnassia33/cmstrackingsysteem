<?php

namespace App\Http\Controllers;
use App\Http\Requests\BlogRequest;
use App\Http\Requests\SubstatusDescriptionRequest;
use App\Models\Blog;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['only' => 'create']);
    }

    public function index()
    {
        $search = \Request::get('search');  //the param of URI
        $blog = Blog::where('question', 'like', '%'.$search.'%')
            ->orderBy('urgency')
            ->paginate();
        return view('blog.index', compact('blog'));

    }
    public function destroy($id)
    {
        Blog::find($id)->delete();

        return redirect()->route('blog.index')
            ->with('success','Question deleted successfully');
    }
//
    public function create()
    {
        return view('blog.create');
    }
//

    /**
     * Store with regex
     *
     * @param BlogRequest $request
     * @return RedirectResponse
     */

    public function store(BlogRequest $request)
    {
        Blog::create($request->all());

        return redirect()->route('blog.index')
            ->with('success','Question created successfully.');
    }

    public function edit($id)
    {
        $blog = Blog::find($id);
        return view('blog.edit',compact('blog'));
    }
    public function show($id)
    {
        $blog = Blog::find($id);
        return view('blog.show',compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(Request $request,$id)
    {
        Blog::find($id)->update($request->all());

        return redirect()->route('blog.index')
            ->with('success','Blog Description updated successfully');
    }
}
