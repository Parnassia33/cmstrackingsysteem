<?php

namespace App\Http\Controllers;

use App\Http\Requests\SubStatusRequest;
use App\Status;
use App\Substatus;
use Illuminate\Http\Request;

class subStatusController extends Controller
{
    public function index()
    {
        $substatuses = Substatus::paginate(5);
//            ->with('tags')
//            ->orderBy('id', 'ASC')
//            ->paginate(3);
        return view('substatus.index', compact('substatuses'));
    }

    public function destroy($id)
    {
        Substatus::find($id)->delete();

        return redirect()->route('substatus.index')
            ->with('success','Product deleted successfully');
    }

    public function create(Status $status)
    {
        $status = Status::all();
        return view('substatus.create', compact('status'));
    }

    /**
     * Store with regex
     *
     * @param  \App\Http\Requests\SubStatusRequest  $request
     * @return Illuminate\Http\Response
     */

    public function store(SubStatusRequest $request)
    {
        Substatus::create($request->all());

        return redirect()->route('substatus.index')
            ->with('success','Substatus created successfully.');
    }

    public function edit($id)
    {
        $substatus = Substatus::find($id);
        return view('substatus.edit',compact('substatus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(Request $request,$id)
    {
        Substatus::find($id)->update($request->all());

        return redirect()->route('substatus.index')
            ->with('success','Item updated successfully');
    }

}
