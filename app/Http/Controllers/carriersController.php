<?php

namespace App\Http\Controllers;

use App\Carrier;
use App\Country;
use GuzzleHttp\ClientTrait;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Illuminate\Support\Facades\Http;
use Spatie\QueryBuilder\QueryBuilder;

class carriersController extends Controller
{
    //
    public function index()
    {
        $carriers = Carrier::paginate(5);
//        $stories = Story::where('user_id', auth()->user()->id)
//            ->with('tags')
//            ->orderBy('id', 'ASC')
//            ->paginate(3);
        return view('admin.index',compact('carriers'));
    }

    public function create()
    {
//        $this->authorize('create');
//        $story = new Story;
//        $tags = Tag::get();
        $countries = Country::all();
        return view('admin.create', compact('countries'));
    }

    public function store(Request $request)
    {
        Carrier::create($request->all());

        return redirect()->route('admin.index')
            ->with('success','Product created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return Application|Factory|Response|View
     */

    public function show($id)
    {
        $carrier = Carrier::find($id);
        return view('admin.show' ,[
        'carrier' => $carrier
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return Application|Factory|Response|View
     */
    public function edit($id)
    {
        $carrier = Carrier::find($id);
        $countries = Country::all();
        return view('admin.edit',compact('carrier','countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function update(Request $request,$id)
    {
        Carrier::find($id)->update($request->all());

        return redirect()->route('admin.index')
            ->with('success','Item updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Carrier $carrier
     * @param Request $request
     * @return RedirectResponse
     */
    public function destroy($id)
    {
        Carrier::find($id)->delete();

        return redirect()->route('admin.index')
            ->with('success','Product deleted successfully');
    }

    /**
     * Create or update a record matching the attributes, and fill it with values.
     *
     * @param Request $request
     * @return Model
     */
    public function getApi(Request $request){
        $token = '3023e896-2e50-4ebb-80a9-8923e1fb47b8';
        $response = Http::withHeaders([
            'Accept' => '*/*',
            'Trackingmore-Api-Key' => $token,
            'Content-Type'=> 'application/json'
        ])->get('https://api.trackingmore.com/v2/carriers');
        $jsonDataFromApi = $response->json();
        $jsonData =$jsonDataFromApi['data'];
        foreach ($jsonData as $item){
            Carrier::updateOrCreate(['name' => $item['name'],
                'code' => $item['code'],
                'country'=> $item['country_code'],
                'type' => $item['type'],
                'homepage' => $item['homepage'],
                'phone' => $item['phone'],
                'picture' => $item['picture'],
            ]);
        }
        return redirect()->route('admin.index')
            ->with('success','All Carriers has been added!');
    }
}
