<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Carrier extends Model
{
    protected $fillable = [
        'name', 'code','country','type','homepage','phone','picture'
    ];

}
