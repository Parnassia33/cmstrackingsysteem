<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tracking_number extends Model
{
    use HasFactory;
    protected $fillable = [
        'tracking_number','carrier','status','substatus','date','statusDescription','destination_info','lastUpdateTime','lastEvent' , 'orderStatus' , 'orderFulfillment' , 'orderDate'
    ];
}
