<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Substatus extends Model
{
    //
    protected $fillable = [
        'substatuscode','status_code','description'
    ];
}
