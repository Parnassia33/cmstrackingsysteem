<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use App\Substatus;
class SubstatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $substatus = [
            [
                'id'         => 1,
                'status_code'       => 'notfound',
                'substatuscode' => 'notfound001',
                'description' => 'The package is waiting for courier to pick up',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),

            ],
            [
                'id'         => 2,
                'status_code'       => 'notfound',
                'substatuscode' => 'notfound002',
                'description' => 'No tracking information found',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'         => 3,
                'status_code'=> 'transit',
                'substatuscode'       => 'transit001',
                'description'=> 'Package is on the way to destination',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'         => 4,
                'status_code'       => 'transit',
                'substatuscode' => 'transit002',
                'description' => 'Package arrived at a hub or sorting center',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'         => 5,
                'status_code'       => 'transit',
                'substatuscode' => 'transit003',
                'description' => '	Package arrived at delivery facility',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'         => 6,
                'status_code'       => 'transit',
                'substatuscode' => 'transit004',
                'description' => '	Package arrived at destination country',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'         => 7,
                'status_code'       => 'transit',
                'substatuscode' => 'transit005',
                'description' => '	Customs clearance completed',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'         => 8,
                'status_code'       => 'delivered',
                'substatuscode' => 'delivered001',
                'description' => '	Package delivered successfully',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'         => 9,
                'status_code'       => 'delivered',
                'substatuscode' => 'delivered002',
                'description' => '	Package picked up by the addressee',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'         => 10,
                'status_code'       => 'delivered',
                'substatuscode' => 'delivered003',
                'description' => '	Package received and signed by addressee',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'         => 11,
                'status_code'       => 'delivered',
                'substatuscode' => 'delivered004',
                'description' => 'Package was left at the front door or left with your neighbour',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'         => 12,
                'status_code'       => 'exception',
                'substatuscode' => 'exception004',
                'description' => 'The package is unclaimed',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'         => 13,
                'status_code'       => 'exception',
                'substatuscode' => 'exception005',
                'description' => '	Other exceptions',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'         => 14,
                'status_code'       => 'exception',
                'substatuscode' => 'exception006',
                'description' => 'Package was detained by customs',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'         => 15,
                'status_code'       => 'exception',
                'substatuscode' => 'exception007',
                'description' => 'Package was lost or damaged during delivery',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'         => 16,
                'status_code'       => 'exception',
                'substatuscode' => 'exception008',
                'description' => 'Logistics order was cancelled before courier pick up the package',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'         => 17,
                'status_code'       => 'exception',
                'substatuscode' => 'exception009',
                'description' => '	Package was refused by addressee',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'         => 18,
                'status_code'       => 'exception',
                'substatuscode' => 'exception0010',
                'description' => 'Package has been returned to sender',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ],
            [
                'id'         => 19,
                'status_code'       => 'exception',
                'substatuscode' => 'exception0011',
                'description' => 'Package is beening sent to sender',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),            ],

        ];

        Substatus::insert($substatus);
    }
}
