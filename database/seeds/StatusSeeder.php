<?php

namespace Database\Seeders;

use App\Status;
use Illuminate\Database\Seeder;
use Carbon\Carbon;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $status = [
            [
                'id'         => 1,
                'code' => 'notfound',
                'description' => 'Package tracking information is no available yet',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),

            ],
            [
                'id'         => 2,
                'code' => 'pending',
                'description' => 'New package added that are pending to track',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),

            ],
            [
                'id'         => 3,
                'code' => 'transit',
                'description' => 'Courier has picked up package from shipper, the package is on the way to destination',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id'         => 4,
                'code' => 'pickup',
                'description' => 'Also known as "Out For Delivery", courier is about to deliver the package, or the package is wating for addressee to pick up',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),

            ],
            [
                'id'         => 5,
                'code' => 'delivered',
                'description' => 'The package was delivered successfully',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),

            ],
            [
                'id'         => 6,
                'code' => 'expired',
                'description' => 'No tracking information for 30days for express service, or no tracking information for 60 days for postal service since the package added',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),

            ],
            [
                'id'         => 7,
                'code' => 'undelivered',
                'description' => 'Also known as "Failed Attempt", courier attempted to deliver but failded, usually left a notice and will try to delivery again',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),

            ],
            [
                'id'         => 8,
                'code' => 'exception',
                'description' => 'Package missed, addressee returned package to sender or other exceptions',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),

            ],


        ];

        Status::insert($status);
    }
}
