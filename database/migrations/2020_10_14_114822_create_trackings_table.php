<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrackingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trackings', function (Blueprint $table) {
            $table->id()->unsigned();
            $table->string('email')->nullable();
            $table->string('orderNumber')->nullable();
            $table->string('orderFulfillment')->nullable();
            $table->string('trackingNumber')->nullable();
            $table->string('carrier')->nullable();
            $table->string('productTitle')->nullable();
            $table->string('productImage', 255)->nullable();
            $table->string('productVariation')->nullable();
            $table->timestamp('createdAt');
            $table->string('lastEvent')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trackings');
    }
}
