<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubstatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('substatuses', function (Blueprint $table) {
            $table->integer('id')->autoIncrement();
            $table->string('status_code');
            $table->foreign('status_code')->references('code')->on('carriers_status');
            $table->string('substatuscode');
            $table->string('description');
            $table->timestamp('updated_at');
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('substatuses');
    }
}
