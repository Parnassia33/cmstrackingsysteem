<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TrackingNumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracking_numbers', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('tracking_number');
            $table->string('carrier');
            $table->string('status')->nullable();
            $table->string('substatus')->nullable();
            $table->string('statusDescription')->nullable();
            $table->string('date')->nullable();
            $table->string('details')->nullable();
            $table->string('destination_info')->nullable();
            $table->string('lastUpdateTime')->nullable();
            $table->string('orderFulfillment')->nullable();
            $table->string('lastEvent')->nullable();
            $table->string('orderStatus')->nullable();
            $table->string('orderDate')->nullable();
            $table->timestamp('updated_at');
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
