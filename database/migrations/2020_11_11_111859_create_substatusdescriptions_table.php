<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubstatusdescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('substatusdescriptions', function (Blueprint $table) {
            $table->id();
            $table->string('substatuscode');
            $table->foreign('substatuscode')->references('code')->on('substatuses');
            $table->string('description');
            $table->integer('percentage');
            $table->integer('hours');
            $table->timestamp('updated_at');
            $table->timestamp('created_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('substatusdescriptions');
    }
}
